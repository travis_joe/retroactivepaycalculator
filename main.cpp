/*
 * Travis Joe
 * CS 2
 * Assignment 2.3 - Pay Increase
 */

#include <iostream>

using namespace std;

int main() {
    double annualSalary = 0;
    const double PAY_INCREASE_PERCENT = 1.076;  // 7.6% pay increase as a decimal
    int monthsRetroactivePay = 0;

    double newAnnualSalary = 0;
    double newMonthlySalary = 0;
    double salaryIncrease = 0;          // yearly salary increase
    double totalRetroactivePay = 0;

    char shouldContinue;                // Repeat after the first run?

    // Format output to 2 decimal places
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    do {
        while (annualSalary <= 0) {
            cout << "Enter your annual salary: ";
            cin >> annualSalary;

            if (annualSalary <= 0) {
                cout << "Annual salary must be greater than 0!";
            }
        }

        while (monthsRetroactivePay <= 0) {
            cout << "Enter the number of months to receive retroactive pay: ";
            cin >> monthsRetroactivePay;

            if (monthsRetroactivePay <= 0) {
                cout << "Months of retroactive pay must be greater than 0!";
            }
        }

        newAnnualSalary = PAY_INCREASE_PERCENT * annualSalary;
        newMonthlySalary = newAnnualSalary / 12;
        salaryIncrease = newAnnualSalary - annualSalary;
        totalRetroactivePay = (salaryIncrease / 12) * monthsRetroactivePay; // monthly salary increase * months of retroactive pay

        cout << "New annual salary: " << newAnnualSalary << "\n";
        cout << "New monthly salary: " << newMonthlySalary << "\n";
        cout << "Salary increase: " << salaryIncrease << "\n";
        cout << "Retroactive pay for " << monthsRetroactivePay << " months: " << totalRetroactivePay << "\n";

        annualSalary = 0;               // Reset previous input before running again
        monthsRetroactivePay = 0;

        cout << "Enter \'y\' to run again, or any other key to exit: ";
        cin >> shouldContinue;
    } while (shouldContinue == 'y' || shouldContinue == 'Y');

    return 0;
}